// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: deep-gray; icon-glyph: calendar-alt;

/*
    Today in WoW - iOS Scriptable app widget that parses information from Wowhead's Today in WoW section

    This widget requires 'cheerio.js' from the same repository to be next to the widget script itself.

    Version: 1.2.0
    Get latest updates at: https://widgets.vildravn.dev/ or https://gitlab.com/vildravn/today-in-wow-widget
    In case something breaks, open an issue at: https://gitlab.com/vildravn/today-in-wow-widget/-/issues

    Changelog: https://gitlab.com/vildravn/today-in-wow-widget/-/blob/beta/CHANGELOG.md
*/

/*
    Colors
*/
const colors = {
    bg: new Color('#1C1C1C'),
    textWhite: new Color('#FFFFFF'),
    textYellow: new Color('#FFD100'),
    textGrey: new Color('#BBBBBB'),
    faction: {
        alliance: new Color('#4d6699'),
        aorde: new Color('#8a2e2e'),
        both: new Color('#333333')
    }
}

/*
    Text Style
*/
const textStyles = {
    header: {
        font: new Font('HelveticaNeue-Bold', 12),
        color: colors.textWhite
    },
    line: {
        font: new Font('HelveticaNeue', 12),
        color: colors.textYellow
    },
    lineBold: {
        font: new Font('HelveticaNeue-Bold', 9),
        color: colors.textGrey
    },
    lineWhite: {
        font: new Font('HelveticaNeue', 12),
        color: colors.textWhite
    }
}

/*
    Shadowlands world boss lookup table
*/
const slBosses = {
    Mortanis: 'https://www.wowhead.com/npc=173104/mortanis#drops',
    'Nurgash Muckformed': 'https://www.wowhead.com/npc=167526/nurgash-muckformed#drops',
    'Oranomonos the Everbranching': 'https://www.wowhead.com/npc=167527/oranomonos-the-everbranching#drops',
    'Valinor': 'https://www.wowhead.com/npc=167524/valinor#drops'
}

// -----------------------------
let REGION = args.widgetParameter || 'EU';
REGION = (REGION.toUpperCase() == 'US' ? 'US' : 'EU');

const cheerio = importModule('cheerio');
let $ = undefined;

if (config.runsInWidget) {
    const size = config.widgetFamily;
    const widget = await createWidget(size);

    Script.setWidget(widget);
    Script.complete();
} else {
    // For debugging
    //const size = 'small';
    const size = 'medium'
    //const size = 'large'
    const widget = await createWidget(size);
    if (size == 'small') {
        widget.presentSmall();
    } else if (size == 'medium') {
        widget.presentMedium();
    } else {
        widget.presentLarge();
    }
    Script.complete();
}

async function createWidget(size) {
    const widget = new ListWidget();
    widget.setPadding(15, 15, 15, 15);

    widget.backgroundColor = colors.bg;

    if (size == 'medium') {
        if (REGION) {
            widget.url = (REGION == 'US' ? 'https://www.wowhead.com/world-quests/sl/na' : 'https://www.wowhead.com/world-quests/sl/eu');

            let whData = await fetchData("https://www.wowhead.com", "loadString");
            $ = cheerio.load(whData);
            let data = null;

            whData = undefined;
            delete (whData);

            const wrapperStack = widget.addStack();
            wrapperStack.layoutHorizontally();
            widget.addSpacer();

            const leftColumn = wrapperStack.addStack();
            leftColumn.layoutVertically();
            data = await parseTextGroup(`#${REGION}-group-calling-quests`, `.tiw-region[data-region="${REGION}"]`);
            await buildTextDisplay(leftColumn, "calling quests", data, 'https://www.wowhead.com/guides/covenant-callings-shadowlands');
            leftColumn.addSpacer(20);
            data = await parseTextGroup(`#${REGION}-group-torghast-wings`, `.tiw-region[data-region="${REGION}"]`);
            await buildTextDisplay(leftColumn, "torghast wings", data, 'https://www.wowhead.com/guides/torghast-tower-of-the-damned-overview');

            wrapperStack.addSpacer();

            const rightColumn = wrapperStack.addStack();
            rightColumn.layoutVertically();
            data = await parseImageGroup(`#${REGION}-group-mythicaffix`, `.tiw-region[data-region="${REGION}"]`);
            await buildAffixDisplay(rightColumn, data);
            rightColumn.addSpacer(20);
            data = await parseTextGroup(`#${REGION}-group-epiceliteworldsl`, `.tiw-region[data-region="${REGION}"]`);
            const bossLink = (data ? slBosses[data[0]] : null);
            await buildTextDisplay(rightColumn, "world boss", data, bossLink);

            $ = undefined;
            delete ($);
        } else {
            const text = widget.addText('The widget is not configured correctly.');
            text.textColor = textStyles.lineWhite.color;
            text.font = textStyles.lineWhite.font;
        }
    } else {
        const contentStack = widget.addStack();
        contentStack.layoutHorizontally();
        contentStack.addSpacer();
        const text = contentStack.addText(`${size} widget is currently not supported.`);
        text.textColor = textStyles.lineWhite.color;
        text.font = textStyles.lineWhite.font;
        contentStack.addSpacer();
    }

    return widget;
}

async function buildAffixDisplay(parent, affixes) {
    const levels = ["2+", "4+", "7+", "10+"];
    if (affixes) {
        for (let i = 0; i < affixes.length; i++) {
            let affix = affixes[i];

            affix.text = levels[i];
        }
    }

    await buildImageDisplay(parent, "mythic+ affixes", affixes);
}

async function buildTextDisplay(parent, heading, data, url) {
    const contentStack = parent.addStack();
    contentStack.layoutVertically();
    contentStack.url = url;

    const header = contentStack.addText(heading.toUpperCase());
    header.textColor = textStyles.header.color;
    header.font = textStyles.header.font;

    if (!data) {
        const line = contentStack.addText('No data');
        line.textColor = textStyles.lineWhite.color;
        line.font = textStyles.lineWhite.font;
        return;
    }

    for (const item of data) {
        const line = contentStack.addText(item);
        line.textColor = textStyles.line.color;
        line.font = textStyles.line.font;
    }
}

async function buildImageDisplay(parent, heading, data) {
    const contentStack = parent.addStack();
    contentStack.layoutVertically();

    const header = contentStack.addText(heading.toUpperCase());
    header.textColor = textStyles.header.color;
    header.font = textStyles.header.font;

    if (!data) {
        const line = contentStack.addText('No data');
        line.textColor = textStyles.lineWhite.color;
        line.font = textStyles.lineWhite.font;
        return;
    }

    const horizontalStack = contentStack.addStack();
    horizontalStack.layoutHorizontally();


    for (const item of data) {
        const image = await fetchData(item.imageUrl, 'loadImage');
        horizontalStack.addSpacer(2);
        buildImageItem(horizontalStack, image, item);
        horizontalStack.addSpacer(2);
    }
}

function buildImageItem(parent, image, item) {
    const contentStack = parent.addStack();
    contentStack.layoutVertically()

    const icon = contentStack.addImage(image);
    icon.borderWidth = 4;
    icon.cornerRadius = 4;
    icon.imageSize = new Size(34, 34);
    icon.borderColor = colors.faction[item.side || "both"];

    if (item.urlFragment) {
        icon.url = `https://wowhead.com${item.urlFragment}`;
    }

    if (item.text) {
        const textStack = contentStack.addStack();
        textStack.layoutHorizontally();
        textStack.size = new Size(34, 0)

        textStack.addSpacer();
        const line = textStack.addText(item.text);
        line.textColor = textStyles.lineBold.color;
        line.font = textStyles.lineBold.font;
        textStack.addSpacer();
    }
}

async function parseImageGroup(widgetSelector, context, side = "both") {
    var returnData = [];
    const widget = $(widgetSelector, context)[0];

    if (!widget)
        return null;

    const widgetLines = $('.tiw-line-name', widget);
    const widgetLineCount = $(widgetLines).length;

    for (let i = 0; i < widgetLineCount; i++) {
        let faction = $(widgetLines)[i].attribs['data-side'];

        if (faction && faction != "both" && faction != side)
            continue;

        if (!faction)
            faction = 'both';

        const lineLink = $('a', widgetLines[i]);
        const urlFragment = lineLink[0].attribs.href;
        const imageUrl = $('img', lineLink[0])[0].attribs.src;

        returnData.push({ imageUrl, urlFragment, faction });
    }

    return returnData;
}

async function parseTextGroup(widgetSelector, context, side = "both") {
    var returnData = [];
    const widget = $(widgetSelector, context)[0];

    if (!widget)
        return null;

    const widgetLines = $(`.tiw-line-name`, widget);
    const widgetLineCount = $(widgetLines).length;

    for (let i = 0; i < widgetLineCount; i++) {
        let faction = $(widgetLines)[i].attribs['data-side'];

        if (faction && faction != "both" && faction != side)
            continue;

        if (!faction)
            faction = 'both';

        returnData.push($(widgetLines[i]).text());
    }

    return returnData;
}

async function fetchData(url, type = 'loadJSON') {
    const req = new Request(url);
    const resp = await req[type]();
    return resp;
}
