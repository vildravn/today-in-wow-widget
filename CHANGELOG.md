# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),

## [1.2.0] - 2020-12-15
### Added
* Added links to elements other than M+, the elements can be tapped now to open a link in the browser
    * Callings - Wowhead Callings guide
    * Torghast Wings - Wowhead Torghast guide
    * World Boss - Loot table of the specific world boss
    * Widget (Empty space) - World quest map for the region

### Changed
* Widget is now configured via widgetParameters, default region is EU

### Fixed
* Fixed an error with wowhead missing data

### Removed
* Removed the FACTION setting as it serves no purpose currently

## [1.1.0] - 2020-12-11
### Changed
* Made some changes to hopefully reduce the memory usage by the widget
* The script now uses cheerio.js directly

### Removed
* scriptable-require.js is no longer needed and was removed

## [1.0.0] - 2020-12-10
### Added
* Initial version of the Today in WoW widget script.
    * The widget displays:
        * Three current Shadowlands Covenant calling quests
        * The two active Torghast wings
        * Current weekly Mythic+ affixes
        * The current weekly Shadowlands world boss
* scriptable-require.js dependency
