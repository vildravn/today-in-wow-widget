![Today in WoW Widget screenshot](https://widgets.vildravn.dev/home/tiw.jpeg)

Today in WoW parses the [Today in WoW section on wowhead.com](https://www.wowhead.com/#tiw-switcher-region) and displays the data as a configurable widget.

The widget displays:
* Active/available Shadowlands Covenant calling quests
* Active Torghast wings
* Current weekly Mythic+ affixes
* Current weekly Shadowlands world boss

The individual elements can also be tapped for more information, leading either to a wowhead guide when tapping on the Calling quests and Torghast wings, the wowhead page of the tapped affix or the world boss' loot table. Tapping on an empty space on the widget brings up a map of current world quests for your region.

### Installation

If you are downloading the scripts with ScriptDude, make sure to click on the Setup Instructions button first.

#### Cheerio

The Today in WoW widget is dependent on a module called Cheerio. You will need to install this module first, for the widget to work.

<a href="https://scriptdu.de/?name=cheerio&source=https%3A%2F%2Fgitlab.com%2Fvildravn%2Ftoday-in-wow-widget%2F-%2Fraw%2Fmaster%2Fcheerio.js&docs=https%3A%2F%2Fwidgets.vildravn.dev%2F%23tiw#generator" target="_blank"><img alt="Download with ScriptDude" src="https://scriptdu.de/download.svg"></a>

or

Download Cheerio from <a href="https://gitlab.com/vildravn/today-in-wow-widget/-/raw/master/cheerio.js" target="_blank">GitLab</a> and place it into the Scriptable folder on your iCloud Drive.

#### Today in WoW

After cheerio is installed in Scriptable, you can install the Today in WoW widget script.

<a href="https://scriptdu.de/?name=Today-in-WoW&source=https%3A%2F%2Fgitlab.com%2Fvildravn%2Ftoday-in-wow-widget%2F-%2Fraw%2Fmaster%2FToday%2520in%2520WoW.js&docs=https%3A%2F%2Fwidgets.vildravn.dev%2F%23tiw#generator" target="_blank"><img alt="Download with ScriptDude" src="https://scriptdu.de/download.svg"></a>

or

Download Today in WoW from <a href="https://gitlab.com/vildravn/today-in-wow-widget/-/raw/master/Today%20in%20WoW.js" target="_blank">GitLab</a> and place it into the Scriptable folder on your iCloud Drive.

After the script is installed within Scriptable:
1. Add a new Scriptable widget to your Home Screen (**Medium size only, for now**)
1. Tap on the added widget and select **Today in WoW** in the **Script** field. You can leave **When Interacting** as-is.
1. Configure the **Parameter** field:
    * Enter your region, the field accepts EU or US (<a href="https://widgets.vildravn.dev/home/tiw_config.png" target="_blank">click for screenshot</a>)
    * If the field is left empty, or is set improperly, the widget will default to displaying EU data
1. Tap away from the Widget configuration modal
1. You may need to wait a bit for iOS to update the widget

[Changelog](https://gitlab.com/vildravn/today-in-wow-widget/-/blob/master/CHANGELOG.md)
